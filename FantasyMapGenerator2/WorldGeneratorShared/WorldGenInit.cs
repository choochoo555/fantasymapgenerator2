﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WorldTerrainShared;

namespace WorldGeneratorShared
{
    public class WorldGenInit
    {
        private WorldProperties _wp = null;
        private WorldBrowser _browser = null;
        private string _savePath = "";
        private string _zipsPath = "";

        public WorldGenInit(WorldProperties wp, string savePath, string zipsPath)
        {
            _wp = wp;
            _savePath = savePath;
            _zipsPath = zipsPath;
        }

        public bool DoneRendering => _wp.MapData != null || (_browser != null && _browser.DoneRendering);

        public void GenerateAndSave(bool force = false)
        {
            var mapFileDir = Path.Combine(_savePath, _wp.FileName);
            var foundFile = File.Exists(mapFileDir);
            byte[] mapData;

            if (!foundFile || force)
            {
                _browser = new WorldBrowser(_wp, _savePath, _zipsPath);
                Task.Run(() => _browser.Inject()).Wait();
                mapData = _browser.GetData();
            }
            else
                mapData = File.ReadAllBytes(mapFileDir);

            _wp.SetMapData(mapData);
        }
    }
}
