﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using PuppeteerSharp;
using TileShared;
using WorldTerrainShared;

namespace WorldGeneratorShared
{
    public class WorldBrowser
    {
        private const string SaveLine = "var fileNameREPLACE_ME_FILE_NAME='REPLACE_ME_FILE_NAME.txt';var REPLACE_ME_FILE_NAME=document.createElement('a'),fileREPLACE_ME_FILE_NAME=new Blob([JSON.stringify(REPLACE_ME_PARAM_NAME)],{type:'text/plain'});REPLACE_ME_FILE_NAME.href=URL.createObjectURL(fileREPLACE_ME_FILE_NAME),REPLACE_ME_FILE_NAME.download=fileNameREPLACE_ME_FILE_NAME,REPLACE_ME_FILE_NAME.click();";
        private const string WaitJS = "window.setTimeout(()=>{REPLACE_ME_LINE},REPLACE_ME_TIME);";
        private const string ReplaceMeFileName = "REPLACE_ME_FILE_NAME";
        private const string ReplaceMeParamName = "REPLACE_ME_PARAM_NAME";

        private const string ReplaceMeLine = "REPLACE_ME_LINE";
        private const string ReplaceMeTime = "REPLACE_ME_TIME";

        private const string FantasyFolderName = ".Fantasy-Map-Generator-master";

        private string _saveDir = "";
        private string _zipsDir = "";

        private WorldProperties _wp = null;

        private bool _doneRendering = false;

        public bool DoneRendering => _doneRendering;

        public WorldBrowser(WorldProperties wp, string saveHDirectory, string zipsDirectory)
        {
            _wp = wp;
            _saveDir = saveHDirectory;
            _zipsDir = zipsDirectory;
        }

        public async Task Inject()
        {
            VerifyExistanceOfMapFiles(_zipsDir);
            ExecuteReplacements();

            var fakeHeights = await RemoveIds();

            //await ModifyPage();
            //Dispose();
            var realHeights = GetRealHeights();
            MergeResults(realHeights, fakeHeights);
            _doneRendering = true;
        }

        private int[] GetRealHeights()
        {
            var hLocation = Path.Combine(_saveDir + @"\h.txt");
            var data = File.ReadAllText(hLocation);
            data = data.Substring(1, data.Length - 2);
            var realHeights = data.Split(',').Select(Int32.Parse).ToArray();
            return realHeights;
        }

        private void VerifyExistanceOfMapFiles(string absolutePathToZips)
        {
            if (Directory.Exists(Path.Combine(absolutePathToZips, FantasyFolderName)))
                return;

            var path = Path.Combine(absolutePathToZips, ".Fantasy-Map-Generator-master.9.zip");
            ZipFile.ExtractToDirectory(path, absolutePathToZips);
        }

        private void VerifyExistanceOfBrowserFiles()
        {
            var path = Path.Combine(_zipsDir, ".Chrome.zip");
            ZipFile.ExtractToDirectory(path, _zipsDir);
        }

        public byte[] GetData()
        {
            return File.ReadAllBytes(Path.Combine(_saveDir + @"\" + _wp.FileName));
        }

        private Browser SetupBrowser()
        {
            string[] files = Directory.GetFiles(_zipsDir, "chrome.exe", SearchOption.AllDirectories);

            if (!files.Any())
            {
                VerifyExistanceOfBrowserFiles();
                files = Directory.GetFiles(_zipsDir, "chrome.exe", SearchOption.AllDirectories);
            }

            var task = Task.Run(async () => await GetBrowser(files));

            return task.Result;
        }

        private async Task<Browser> GetBrowser(string[] files)
        {
            return await Puppeteer.LaunchAsync(new LaunchOptions
            {
                Headless = true,
                Args = new[] { "--single-instance" },
                ExecutablePath = files.FirstOrDefault(),
                DefaultViewport = new ViewPortOptions()
                {
                    Width = _wp.Width,
                    Height = _wp.Height
                }
            });
        }

        private async Task<Page> ModifyPage()
        {
            var browser = SetupBrowser();
            var page = await browser.NewPageAsync();

            var client = await page.Target.CreateCDPSessionAsync();

            if (!Directory.Exists(_saveDir))
                Directory.CreateDirectory(_saveDir);

            await client.SendAsync("Page.setDownloadBehavior", new
            {
                behavior = "allow",
                downloadPath = _saveDir,
            });

            await page.GoToAsync(PathCombine(Path.Combine(_zipsDir, FantasyFolderName), "index.html"));
            return page;
        }

        private void MergeResults(int[] realHeights, byte[] fakeHeights)
        {
            var output = new byte[fakeHeights.Length];
            for (var x = 0; x < _wp.Width; x++)
            {
                for (var y = 0; y < _wp.Height; y++)
                {
                    var realVal = realHeights[y * _wp.Width + x];
                    var fakeVal = fakeHeights[y * _wp.Width + x];
                    int newVal = fakeVal;

                    if (realVal < -50)
                        newVal = WorldTerrain.OceanDeepByte;//deep
                    else if (realVal < -10)
                        newVal = WorldTerrain.OceanByte;//ocean
                    else if (realVal < 0)
                        newVal = WorldTerrain.OceanShallowByte;//shallow
                    else if (realVal < 10)
                        newVal = WorldTerrain.BeachByte;//beach
                    else if (realVal < 4000)
                        newVal = WorldTerrain.GrassByte;//grass
                    else if (realVal < 6000)
                        newVal = WorldTerrain.HillByte;//hill
                    else
                        newVal = WorldTerrain.MountainByte;//hill

                    if (fakeVal < newVal)
                        newVal = fakeVal;

                    if (6 < newVal)
                        newVal = fakeVal;

                    output[y * _wp.Width + x] = (byte)newVal;
                }
            }


            if (File.Exists(PathCombine(_saveDir, @"\h.txt")))
                File.Delete(PathCombine(_saveDir, @"\h.txt"));
            //Exceptions in Saving
            DeepOceanAlwaysByOcean(output);

            File.WriteAllBytes(PathCombine(_saveDir, _wp.FileName), output);
        }

        private void DeepOceanAlwaysByOcean(byte[] output)
        {
            for (var x = 0; x < _wp.Width; x++)
            {
                for (var y = 0; y < _wp.Height; y++)
                {
                    var val = output[y * _wp.Width + x];
                    if (val != WorldTerrain.OceanShallowByte)
                        continue;

                    var neighbors = TileTools.GetNeighborsOneD<byte?, byte>(_wp.Width, _wp.Height, output, x, y);
                    if (neighbors.Any(n => n.HasValue && n.Value == WorldTerrain.OceanDeepByte))
                        output[y * _wp.Width + x] = (byte)WorldTerrain.OceanByte;
                }
            }
        }

        private async Task<byte[]> RemoveIds()
        {
            var page = await ModifyPage();

            // await _page.ev('a', a => a.href)
            var listOfElementsToDelete = new List<string>()
            {
                "oceanPattern",
                "lakes",
                "rivers",
                "texture",
                "biomes",
                "cells",
                "gridOverlay",
                "coordinates",
                "compass",
                "cults",

                "oceanBase",
                "loading",
                "initial",
                "scaleBar",
                "arrow",
                "rose",
                "oceanic",
                //"landmass",
                "terrain",
                "regions",
                "borders",
                "debug",
                "routes",
                "ruler",
                "temperature",
                "markers",
                "icons",
                "labels",
                "coastline",
                "prec",
                "population",
                "optionsContainer",
                "dialogs",
                "map-dragged",
                "legend",
                "tooltip",
                "fileInputs"
            };
            foreach (var elem in listOfElementsToDelete)
            {
                await page.EvaluateExpressionAsync($"var element=document.getElementById('{elem}');if(element)element.parentNode.removeChild(element);else console.log('{elem}')");
            }
            byte[] elevations = null;
            using (MemoryStream memstr = new MemoryStream(await page.ScreenshotDataAsync()))
            {
                Image img = Image.FromStream(memstr);
                var origBM = new Bitmap(img);
                elevations = GetElevation(origBM, 7);
                origBM.Dispose();
                img.Dispose();
                //File.WriteAllBytes(PathCombine(_dataDir, "fakeh.txt"), elevations);
            }
            DisposePage(page.Browser);
            return elevations;
        }
        private void DisposePage(Browser browser)
        {
            //var browser = page.Browser;
            //page.Dispose();
            Task.Run(async () => await browser.CloseAsync());
            browser.Dispose();
        }

        private byte[] GetElevation(Bitmap bm, int numberOfElevations)
        {
            Dictionary<Color, int> histo = new Dictionary<Color, int>();
            for (int x = 0; x < bm.Size.Width; x++)
                for (int y = 0; y < bm.Size.Height; y++)
                {
                    Color c = bm.GetPixel(x, y);   // **1**
                    if (histo.ContainsKey(c)) histo[c] = histo[c] + 1;
                    else histo.Add(c, 1);
                }
            var result1 = histo.OrderByDescending(a => a.Value);
            var mostusedcolor = result1.Select(x => x.Key).Take(numberOfElevations).ToList();

            Double temp;
            Dictionary<Color, Double> dist = new Dictionary<Color, double>();
            Dictionary<Color, Color> mapping = new Dictionary<Color, Color>();
            foreach (var p in result1)
            {
                dist.Clear();
                foreach (Color pp in mostusedcolor)
                {
                    temp = Math.Abs(p.Key.R - pp.R) +
                           Math.Abs(p.Key.R - pp.R) +
                           Math.Abs(p.Key.R - pp.R);
                    dist.Add(pp, temp);
                }
                var min = dist.OrderBy(k => k.Value).FirstOrDefault();
                mapping.Add(p.Key, min.Key);
            }

            var topColors = mapping.Select(x => x.Value).Distinct().OrderBy(x => x.GetBrightness()).ToList();
            var elevation = new byte[bm.Size.Width * bm.Size.Height];
            for (int x = 0; x < bm.Size.Width; x++)
                for (int y = 0; y < bm.Size.Height; y++)
                {
                    Color c = bm.GetPixel(x, y);   // **2**
                    var index = topColors.FindIndex(a => a.Name == mapping[c].Name);
                    elevation[bm.Size.Width * y + x] = (byte)index;
                }

            return elevation;
        }

        private void ExecuteReplacements()
        {
            const int numCultures = 14;
            const int numStates = 15;
            const int numSize = 0;
            const decimal growthRate = 1.8m;
            const string numTowns = "1000";//"auto"

            const string mainFile = @"main.js";
            const string optionsFile = @"\modules\ui\options.js";
            const string layersFile = @"\modules\ui\layers.js";
            const string heightmapFile = @"\modules\ui\options.js";
            const string generalFile = @"\modules\ui\general.js";

            CreateCopy(mainFile);
            CreateCopy(optionsFile);
            CreateCopy(layersFile);
            CreateCopy(heightmapFile);
            CreateCopy(generalFile);

            //stop alllocalStorage
            ExecuteReplacementParam(optionsFile, "function applyStoredOptions() {", "function applyStoredOptions() {mapWidthInput.value = " + _wp.Width + ";mapHeightInput.value = " + _wp.Height + "; return;");
            ExecuteReplacementParam(mainFile, "optionsSeed.value = seed;", $"seed = {_wp.Seed};optionsSeed.value = seed;");

            //Map Dimensions
            ExecuteReplacementParam(generalFile, "mapWidthInput.value = window.innerWidth;", $"mapWidthInput.value = {_wp.Width};");
            ExecuteReplacementParam(generalFile, "mapHeightInput.value = window.innerHeight;", $"mapHeightInput.value = {_wp.Height};");
            ExecuteReplacementParam(optionsFile, "mapWidthInput.value = graphWidth;", $"mapWidthInput.value = {_wp.Width};");
            ExecuteReplacementParam(optionsFile, "mapHeightInput.value = graphHeight;", $"mapHeightInput.value = {_wp.Height};");
            ExecuteReplacementParam(optionsFile, "mapWidthInput.value = mapHistory[id].width;", $"mapWidthInput.value = {_wp.Width};");
            ExecuteReplacementParam(optionsFile, "mapHeightInput.value = mapHistory[id].height;", $"mapHeightInput.value = {_wp.Height};");

            //Params
            ExecuteReplacementParam(heightmapFile, "const input = document.getElementById(\"templateInput\")", $"document.getElementById(\"templateInput\").value='{_wp.MapType}';const input = document.getElementById(\"templateInput\");");
            ExecuteReplacementParam(optionsFile, "culturesInput.value = culturesOutput.value = rand(10, 15);", $"culturesInput.value = culturesOutput.value = {numCultures};");
            ExecuteReplacementParam(optionsFile, "regionsInput.value = regionsOutput.value = rand(12, 17);", $"regionsInput.value = regionsOutput.value = {numStates};");
            ExecuteReplacementParam(optionsFile, "powerInput.value = powerOutput.value = rand(0, 4);", $"powerInput.value = powerOutput.value = {numSize};");
            ExecuteReplacementParam(optionsFile, "neutralInput.value = neutralOutput.value = rn(0.8 + Math.random(), 1)", $"neutralInput.value = neutralOutput.value = {growthRate}");
            ExecuteReplacementParam(optionsFile, "manorsInput.value = 1000; manorsOutput.value = \"auto\"", $"manorsInput.value = {numTowns}; manorsOutput.value = \"auto\"");
            //Map View
            ExecuteReplacementParam(layersFile, "function restoreLayers() {", "function restoreLayers() {drawHeightmap();return;");

            //AddToUtils
            //ExecuteAddToUtils("function flatten(array){return!Array.isArray(array)?array:[].concat.apply([],array.map(flatten));}");
            //  ExecuteAddToUtils("function removeElement(elementId){var element=document.getElementById(elementId);element.parentNode.removeChild(element);}");

            //Saving of pack
            SaveHeight();
            //ExecuteReplacementSaveFile("h", @"main.js", "function generate() {", "pack.cells.h");
            //ExecuteReplacementSaveFile("gridh", @"main.js", "function generate() {", "grid.cells.h");
            //ExecuteReplacementSaveFile("q", @"main.js", "function generate() {", "flatten(pack.cells.q._root).filter(x => x)");

        }

        private void CreateCopy(string location)
        {
            var fantasyLoc = PathCombine(_zipsDir, FantasyFolderName);
            var loc = PathCombine(fantasyLoc, location);
            var locCopy = loc + "COPY";

            //Must be first time, that means original file is unmolested, hehe.
            if (!File.Exists(locCopy))
                File.Copy(loc, locCopy);

            if (File.Exists(loc))
                File.Delete(loc);

            File.Copy(locCopy, loc);
        }

        private void SaveHeight()
        {
            var line = "for(var width=" + _wp.Width + ",height=" + _wp.Height + ",outputArray=[],y=0;y<height;y++)for(var x=0;x<width;x++){let e=findCell(x,y),t=getFriendlyHeight(pack.cells.h[e]);outputArray[y*width+x]=parseInt(t.replace(' ft',''),10)}var fileNameh='h.txt',h=document.createElement('a'),fileh=new Blob([JSON.stringify(outputArray)],{type:'text/plain'});h.href=URL.createObjectURL(fileh),h.download=fileNameh,h.click();";

            ExecuteReplacementParam("main.js", "console.timeEnd(\"TOTAL\");", line);
            /*

                var width = 1000;
                var height = 800;
                var outputArray = [];

            for(var x = 0; x < width; x++){
            for(var y = 0; y < height; y++){                
                let cell = findCell(x, y);
                let myHeight = getFriendlyHeight(pack.cells.h[cell]);
                outputArray[y*width+x] = parseInt(myHeight.replace(" ft",""),10);
                }
                }
                var fileNameh='h.txt';var h=document.createElement('a'),fileh=new Blob([JSON.stringify(outputArray)],{type:'text/plain'});h.href=URL.createObjectURL(fileh),h.download=fileNameh,h.click();
             */
        }

        private void ExecuteAddToUtils(string lineToAdd)
        {
            var lineToStart = "function find(x, y, radius = Infinity) {";
            ExecuteReplacementParam(@"\modules\utils.js", lineToStart, $"{lineToAdd}\n{lineToStart}");
        }

        private string PathCombine(string path1, string path2)
        {
            if (path1 == null) return path2;
            else if (path2 == null) return path1;
            else return path1.Trim().TrimEnd(System.IO.Path.DirectorySeparatorChar)
                        + System.IO.Path.DirectorySeparatorChar
                        + path2.Trim().TrimStart(System.IO.Path.DirectorySeparatorChar);
        }

        private void ExecuteReplacementParam(string location, string oldVal, string newVal)
        {
            var fantasyLoc = PathCombine(_zipsDir, FantasyFolderName);
            var loc = PathCombine(fantasyLoc, location);
            var allText = File.ReadAllText(loc);

            allText = allText.Replace(oldVal, newVal);

            File.WriteAllText(loc, allText);
        }

        private void ExecuteReplacementSaveFile(string fileName, string location, string replaceFunction, string paramName)
        {
            var loc = PathCombine(_zipsDir, location);
            var allText = File.ReadAllText(loc);
            var newLine = SaveLine.Replace(ReplaceMeFileName, fileName);
            newLine = newLine.Replace(ReplaceMeParamName, paramName) + "\n";
            if (allText.Contains(newLine))
                return;

            var count = 1;
            var startText = allText.Substring(allText.IndexOf(replaceFunction) + replaceFunction.Length);
            while (count > 0)
            {
                var nextChar = startText.Substring(0, 1);
                if (nextChar == "{")
                    count++;
                if (nextChar == "}")
                    count--;
                if (count != 0)
                    startText = startText.Substring(1);
            }

            var position = allText.IndexOf(startText);

            //newLine = WaitJS.Replace(ReplaceMeTime, _waitTime.ToString()).Replace(ReplaceMeLine, newLine);
            allText = allText.Substring(0, position) + newLine + allText.Substring(position);
            File.WriteAllText(loc, allText);
        }

    }
}

