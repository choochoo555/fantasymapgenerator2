﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasyMapGenerator2
{
    public static class DownloadFromGithub
    {
        private const string PerminentDir = "bob";
        public static string Start(string urlToZip, string localDestination, string githubPrivateToken = "")
        {
            var pathToUnzippedFolder = string.Empty;
            using (var client = new System.Net.Http.HttpClient())
            {
                if (!string.IsNullOrEmpty(githubPrivateToken))
                {
                    var credentials = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}:", githubPrivateToken);
                    credentials = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(credentials));
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", credentials);
                }

                var contents = client.GetByteArrayAsync(urlToZip).Result;
                var folderName = string.Empty;
                if (string.IsNullOrEmpty(PerminentDir))
                    folderName = Guid.NewGuid().ToString().Replace("-", string.Empty);
                else
                    folderName = PerminentDir;

                var folderLoc = Path.Combine(localDestination, folderName);

                if (!Directory.Exists(folderLoc))
                {

                    Directory.CreateDirectory(folderLoc);

                    var zipPath = Path.Combine(folderLoc, "repo.zip");
                    System.IO.File.WriteAllBytes(zipPath, contents);
                    ZipFile.ExtractToDirectory(zipPath, folderLoc);
                    File.Delete(zipPath);
                }
                var unzippedFolders = Directory.GetDirectories(folderLoc);
                pathToUnzippedFolder = unzippedFolders.FirstOrDefault();
            }
            return pathToUnzippedFolder;
        }
    }
}
