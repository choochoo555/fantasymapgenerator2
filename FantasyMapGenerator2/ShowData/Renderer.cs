﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FantasyMapGenerator2.ShowData
{
    public class Renderer
    {
        // private Grid _grid;
        private byte[] MapData = null;
        private int[] MapDataReal = null;
        private Form _form;
        public Renderer(Form form)
        {
            _form = form;
            // MapData = new int[_form.Width, _form.Height];
        }


        public Color[] MapColors =
        {
            Color.Navy,
            Color.DarkBlue,
            Color.Blue,
            Color.LightBlue,
            Color.Green,
            Color.Tan,
            Color.ForestGreen,
            Color.DarkGreen,
            Color.Brown,
            Color.Yellow,
            Color.White
        };

        public void Start(byte[] md)
        {
            MapData = md;
            _form.Invalidate();
        }

        internal void Paint(Graphics graphics)
        {
            if (MapData == null)
                return;
            //if (MapDataReal == null)
            //  return;

            using (Pen myPen = new Pen(Color.Empty, 1))
            {
                for (var x = 0; x < _form.Width; x++)
                {
                    for (var y = 0; y < _form.Height; y++)
                    {

                        //var heightVal = MapData[y * _form.Width + x];

                        /*if (heightVal < -50)
                        {
                            myPen.Color = Color.Navy;
                        }
                        else if (heightVal < -10)
                        {
                            myPen.Color = Color.Blue;
                        }
                        else if (heightVal < 0)
                        {
                            myPen.Color = Color.LightBlue;
                        }
                        else if (heightVal < 10)
                        {
                            myPen.Color = Color.Yellow;
                        }
                        else if (heightVal < 400)
                        {
                            myPen.Color = Color.Green;
                        }
                        */
                        // myPen.Color = Color.Green;
                        myPen.Color = MapColors[MapData[y * _form.Width + x]];
                        graphics.DrawRectangle(myPen, x, y, 1, 1);

                    }
                }

            }
        }
    }
}
