﻿
using FantasyMapGenerator2.ShowData;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using WorldGeneratorShared;
using WorldTerrainShared;

namespace FantasyMapGenerator2
{

    public class Main
    {
        private Renderer _renderer;
        private WorldGenInit _worldGen;
        private System.Timers.Timer _timer;
        private WorldProperties _wp = null;
        private string _savePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

        public Main(Form form)
        {
            _renderer = new Renderer(form);
            _wp = new WorldProperties(form.Width, form.Height, 804);

            _timer = new System.Timers.Timer();
            _timer.Interval = 50;
            _timer.Elapsed += new System.Timers.ElapsedEventHandler(TimerEventProcessor);
            _timer.Start();

            OrganizeData();
        }

        private void TimerEventProcessor(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (_worldGen == null)
                return;

            if (!_worldGen.DoneRendering)
                return;

            _timer.Stop();
            _renderer.Start(_wp.MapData);
        }

        private void OrganizeData()
        {
            /*
            const string urlToZip = "https://github.com/Choochoo/Fantasy-Map-Generator/archive/master.zip";
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            var downloadLocation = DownloadFromGithub.Start(urlToZip, path);
            */

            var zipsPath = System.IO.Directory.GetCurrentDirectory();
            zipsPath = zipsPath.Substring(0, zipsPath.IndexOf("FantasyMapGenerator2", StringComparison.Ordinal)) + @"Summer2019UnityMapGen\Assets\Plugins\";

            _worldGen = new WorldGenInit(_wp, _savePath, zipsPath);
            _worldGen.GenerateAndSave();
        }

        internal void Paint(Graphics graphics)
        {
            _renderer.Paint(graphics);
        }
    }

}