﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FantasyMapGenerator2
{
    public sealed partial class App : Form
    {
        private Main _main = null;
        public App()
        {
            InitializeComponent();
            DoubleBuffered = true;
            _main = new Main(this);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            _main.Paint(e.Graphics);
        }
    }
}
