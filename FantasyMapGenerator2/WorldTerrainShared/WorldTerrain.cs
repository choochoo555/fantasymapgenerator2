﻿namespace WorldTerrainShared
{
    public static class WorldTerrain
    {
        public static int OceanDeepByte => 0;
        public static int OceanByte => 1;
        public static int OceanShallowByte => 2;
        public static int BeachByte => 3;
        public static int GrassByte => 4;
        public static int HillByte => 5;
        public static int MountainByte => 6;

        public static string OceanDeep => "OceanDeep";
        public static string Ocean => "Ocean";
        public static string OceanShallow => "OceanShallow";
        public static string Beach => "Beach";
        public static string Grass => "Grass";
        public static string Hill => "Hill";
        public static string Mountain => "Mountain";
    }
}