﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldTerrainShared
{
    public class WorldProperties
    {
        public int Width { get; } = 0;

        public int Height { get; } = 0;

        private byte[] _mapData = null;
        public byte[] MapData => _mapData;

        private readonly int _seed = 0;
        public int Seed => _seed;

        private Random _genNum = null;
        public readonly Random GenNum = new Random();

        public string MapType = "Archipelago";

        public WorldProperties(int width, int height, int? seed = null)
        {
            _seed = seed ?? GenNum.Next();

            GenNum = new Random(_seed);

            Width = width;
            Height = height;
        }

        public void SetMapData(byte[] md)
        {
            _mapData = md;
        }

        public string FileName => $"WG_{Width}_{Height}_{Seed}.dat";
    }
}
