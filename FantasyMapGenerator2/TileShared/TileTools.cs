﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileShared
{
    public static class TileTools
    {
        public static T GetTileOneD<T, U>(int mapWidth, int mapHeight, U[] mapdata, int modX, int modY)
        {
            if (modX < 0 || modX >= mapWidth)
                return default(T);
            if (modY < 0 || modY >= mapHeight)
                return default(T);

            var t = typeof(T);

            return (T)Convert.ChangeType(mapdata[modY * mapWidth + modX], Nullable.GetUnderlyingType(t));
        }

        public static T GetTileTwoD<T>(T[][] tileData, int modX, int modY) where T : class
        {
            if (modX < 0 || modX >= tileData.Length)
                return null;
            if (modY < 0 || modY >= tileData[modX].Length)
                return null;

            return tileData[modX][modY];
        }

        public static T[] GetNeighborsOneD<T, U>(int w, int h, U[] mp, int x, int y)
        {
            var neighbors = new T[8];

            neighbors[Sides.NorthWest] = TileTools.GetTileOneD<T, U>(w, h, mp, x - 1, y - 1);
            neighbors[Sides.North] = TileTools.GetTileOneD<T, U>(w, h, mp, x - 0, y - 1);
            neighbors[Sides.NorthEast] = TileTools.GetTileOneD<T, U>(w, h, mp, x + 1, y - 1);

            neighbors[Sides.West] = TileTools.GetTileOneD<T, U>(w, h, mp, x - 1, y - 0);
            neighbors[Sides.East] = TileTools.GetTileOneD<T, U>(w, h, mp, x + 1, y - 0);

            neighbors[Sides.SouthWest] = TileTools.GetTileOneD<T, U>(w, h, mp, x - 1, y + 1);
            neighbors[Sides.South] = TileTools.GetTileOneD<T, U>(w, h, mp, x - 0, y + 1);
            neighbors[Sides.SouthEast] = TileTools.GetTileOneD<T, U>(w, h, mp, x + 1, y + 1);

            return neighbors;
        }

        public static T[] GetNeighborsTwoD<T>(T[][] mp, int x, int y) where T : class
        {
            var neighbors = new T[8];

            neighbors[Sides.NorthWest] = TileTools.GetTileTwoD<T>(mp, x - 1, y - 1);
            neighbors[Sides.North] = TileTools.GetTileTwoD<T>(mp, x - 0, y - 1);
            neighbors[Sides.NorthEast] = TileTools.GetTileTwoD<T>(mp, x + 1, y - 1);

            neighbors[Sides.West] = TileTools.GetTileTwoD<T>(mp, x - 1, y - 0);
            neighbors[Sides.East] = TileTools.GetTileTwoD<T>(mp, x + 1, y - 0);

            neighbors[Sides.SouthWest] = TileTools.GetTileTwoD<T>(mp, x - 1, y + 1);
            neighbors[Sides.South] = TileTools.GetTileTwoD<T>(mp, x - 0, y + 1);
            neighbors[Sides.SouthEast] = TileTools.GetTileTwoD<T>(mp, x + 1, y + 1);

            return neighbors;
        }
    }
}
