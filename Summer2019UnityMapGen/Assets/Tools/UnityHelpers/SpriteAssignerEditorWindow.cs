﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.Tilemaps;

public class SpritesAssignerWindow : EditorWindow
{

    [MenuItem("Window/My Utilities/Sprite Assigner")]
    public static void SpriteAssigner()
    {
        SpritesAssignerWindow spriteAssigner = GetWindow<SpritesAssignerWindow>("Sprites Assigner", true);
        spriteAssigner.Show();
    }

    //name, xoffset, yoffset, columns
    private string[] Folders = new[] { "OceanDeep","Ocean", "OceanShallow", "Grass"};
    private string m_Log;

    private void OnGUI()
    {
        if (GUILayout.Button("Edit sprites"))
            EditSprites();

        m_Log = EditorGUILayout.TextArea(m_Log);
    }

    private void EditSprites()
    {
        List<Sprite> sprites = new List<Sprite>(Array.ConvertAll(AssetDatabase.LoadAllAssetsAtPath("Assets/Overworld Tileset/Tile Spritesheets/Overworld_Tileset.png"), item => item as Sprite));
        sprites = sprites.Where(x => x != null && !string.IsNullOrEmpty(x.name) && !x.name.Contains("NotFound")).ToList();
        // var foundS = sprites.Find(x => x.name == "OceanDeep");
        // Debug.Log("Sprites Length: " + sprites.Count);
        // Debug.Log(foundS==null);
        Debug.Log("Folders:" + string.Join(",",Folders));
        for (var i = 0; i < Folders.Length; i++)
        {
            var folder = Folders[i];
            var path = $"Animated Tiles/{folder}";
            Debug.Log("Loading:" + path);
            var objects = Resources.LoadAll<AnimatedTile>(path);
            var animatedTiles = Array.ConvertAll(objects, item => item as AnimatedTile);

            if(folder == "OceanDeep")//exception
            {
                var animTile = animatedTiles.FirstOrDefault();
                animTile.m_AnimatedSprites = new Sprite[]
                {
                    sprites.Find(x => x.name == "Ocean_0_1")
                };
                continue;
            }

            foreach (var animTile in animatedTiles)
            {
                var name = $"{folder}_{animTile.name}_";
                /*
                if (animTile.name == "0")
                {
                    var prevFolderName = i-1 >= 0 ? Folders[i - 1] : Folders[i];
                    var s = sprites.Find(x => x.name == prevFolderName);
                    animTile.m_AnimatedSprites = new Sprite[] {s};
                    EditorUtility.SetDirty(animTile);
                    continue;
                }

                if (animTile.name == "15")
                {
                    var s = sprites.Find(x => x.name == folder);
                    animTile.m_AnimatedSprites = new Sprite[] { s };
                    EditorUtility.SetDirty(animTile);
                    continue;
                }*/

                var allSprites = new List<Sprite>
                {
                    sprites.Find(x => x.name == name + "1"),
                    sprites.Find(x => x.name == name + "2"),
                    sprites.Find(x => x.name == name + "3"),
                    sprites.Find(x => x.name == name + "4"),
                    sprites.Find(x => x.name == name + "3"),
                    sprites.Find(x => x.name == name + "2")
                };

                animTile.m_AnimatedSprites = allSprites.Where(x => x != null).ToArray();

                EditorUtility.SetDirty(animTile);
            }
        }
    }
}
