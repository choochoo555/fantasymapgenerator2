﻿public class WangTile
{
    public WangTile(int tileNum, int xStart, int yStart, int xMultiplier = 0, int yMultiplier = 0, int count = 4)
    {
        TileNum = tileNum;
        XStart = xStart;
        YStart = yStart;
        XMultiplier = xMultiplier;
        YMultiplier = yMultiplier;
        Count = count;
    }

    public int TileNum { get; set; }

    public int Count { get; set; }

    public int XStart { get; set; }
    public int YStart { get; set; }

    public int XMultiplier { get; set; }
    public int YMultiplier { get; set; }
}