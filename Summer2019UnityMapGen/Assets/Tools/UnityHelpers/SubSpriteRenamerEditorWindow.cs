﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SpriteRenamerWindow : EditorWindow
{

    [MenuItem("Window/My Utilities/Sprite Renamer")]
    public static void SpriteRenamer()
    {
        SpriteRenamerWindow spriteRenamer = GetWindow<SpriteRenamerWindow>("Sprite Renamer", true);
        spriteRenamer.Show();
        if (Selection.activeObject != null && Selection.activeObject.GetType() == typeof(Texture2D))
            spriteRenamer.spritesheet = (Texture2D)Selection.activeObject;
    }

    //name, xoffset, yoffset, columns
    private List<Tuple<string, List<WangTile>>> Terrains;
    public Texture2D spritesheet;
    private string m_Log;

    private void OnGUI()
    {

        spritesheet = EditorGUILayout.ObjectField("Spritesheet", spritesheet, typeof(Texture2D), allowSceneObjects: false) as Texture2D;
        
        using (new EditorGUI.DisabledGroupScope(spritesheet == null))
            if (GUILayout.Button("Edit sprites"))
                EditSprites();

        m_Log = EditorGUILayout.TextArea(m_Log);
    }

#if UNITY_EDITOR
    private void EditSprites()
    {
        if (spritesheet == null)
            return;


        string spritesheetPath = AssetDatabase.GetAssetPath(spritesheet);

        if (string.IsNullOrEmpty(spritesheetPath))
            return;

        TextureImporter importer = AssetImporter.GetAtPath(spritesheetPath) as TextureImporter;

        if (importer == null || importer.spritesheet == null || importer.spriteImportMode != SpriteImportMode.Multiple)
            return;

        CreateTerrains();

        SpriteMetaData[] spritesMetaData = importer.spritesheet;
        for (int i = 0; i < spritesMetaData.Length; i++)
        {
            SpriteMetaData metaData = spritesMetaData[i];
            var newName = GetName(metaData);
            if (string.IsNullOrEmpty(newName))
                newName = "NotFound" + i;
            metaData.name = newName;
            spritesMetaData[i] = metaData;
        }
        importer.spritesheet = spritesMetaData;

        EditorUtility.SetDirty(importer);
        importer.SaveAndReimport();
        m_Log += string.Format("Edited {0} sprites in {1}\n", spritesMetaData.Length, spritesheetPath);
    }
#endif
    private void CreateTerrains()
    {
        Terrains = new List<Tuple<string, List<WangTile>>>();
        Terrains.Add(Ocean());
        Terrains.Add(OceanShallow());
        Terrains.Add(Grass());
    }

    private string GetName(SpriteMetaData md)
    {
        for (var i = 0; i < Terrains.Count; i++)
        {
            var terrain = Terrains[i];
            var nextTerrain = i + 1 < Terrains.Count ? Terrains[i + 1] : null;
            var foundName = ProcessTerrain(md, terrain, nextTerrain);
            if (string.IsNullOrEmpty(foundName))
                continue;
            return foundName;
        }

        return string.Empty;
    }
    private string ProcessTerrain(SpriteMetaData sliceMetaData, Tuple<string, List<WangTile>> terrain, Tuple<string, List<WangTile>> nextTerrain)
    {
        var wangs = terrain.Item2;
        foreach(var wang in wangs)
        {         
            for (var i = 0; i < wang.Count; i++)
            {
                var xOffset = (16 * wang.XMultiplier) * i;
                var yOffset = (16 * wang.YMultiplier) * i;
                xOffset += wang.XStart;
                yOffset += wang.YStart;

                var foundWang = wangs.Find(x => sliceMetaData.rect.x == xOffset && sliceMetaData.rect.y == yOffset);
                if (foundWang == null)
                    continue;

                /*
                if (wang.TileNum == 0)
                    return terrain.Item1;

                if (wang.TileNum == 15 && nextTerrain != null)
                    return nextTerrain.Item1;
                    */
                return $"{terrain.Item1}_{wang.TileNum}_{i + 1}";
            }
        }
        return string.Empty;
    }

    private Tuple<string,List<WangTile>> Ocean()
    {
        var wangTiles = new List<WangTile>();
        const int xStart = 0;
        const int yStart = 288;

        var xStartTemp = xStart;
        var yStartTemp = yStart;
        //rowone
        wangTiles.Add(new WangTile(2, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(6, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(4, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(7, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(14, xStartTemp, yStartTemp, 5));

        //rowtwo
        xStartTemp = xStart;
        yStartTemp = yStart-16;
        wangTiles.Add(new WangTile(3, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(0, 256, 240, 0, 0, 1));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(12, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(11, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(13, xStartTemp, yStartTemp, 5));

        //row three
        xStartTemp = xStart;
        yStartTemp = yStart - 32;
        wangTiles.Add(new WangTile(1, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(9, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(8, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(10, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(5, xStartTemp, yStartTemp, 5));

        wangTiles.Add(new WangTile(15, 16, 272, 0, 0, 1));

        return Tuple.Create("Ocean", wangTiles);
    }

    private Tuple<string, List<WangTile>> OceanShallow()
    {
        var wangTiles = new List<WangTile>();
        const int xStart = 320;
        const int yStart = 288;

        var xStartTemp = xStart;
        var yStartTemp = yStart;
        //rowone
        wangTiles.Add(new WangTile(2, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(6, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(4, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(7, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(14, xStartTemp, yStartTemp, 5));

        //rowtwo
        xStartTemp = xStart;
        yStartTemp = yStart - 16;
        wangTiles.Add(new WangTile(3, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(0, 96, 272, 0, 0, 1));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(12, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(11, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(13, xStartTemp, yStartTemp, 5));

        //row three
        xStartTemp = xStart;
        yStartTemp = yStart - 32;
        wangTiles.Add(new WangTile(1, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(9, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(8, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(10, xStartTemp, yStartTemp, 5));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(5, xStartTemp, yStartTemp, 5));

        wangTiles.Add(new WangTile(15, 336, 272, 0, 0, 1));

        return Tuple.Create("OceanShallow", wangTiles);
    }

    private Tuple<string, List<WangTile>> Grass()
    {
        var wangTiles = new List<WangTile>();
        const int xStart = 0;
        const int yStart = 576;

        var xStartTemp = xStart;
        var yStartTemp = yStart;

        //rowone
        wangTiles.Add(new WangTile(2, xStartTemp, yStartTemp, 1, 1, 1));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(6, xStartTemp, yStartTemp, 1, 1, 1));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(4, xStartTemp, yStartTemp, 1, 1, 1));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(7, xStartTemp, yStartTemp, 1, 1, 1));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(14, xStartTemp, yStartTemp, 1, 1, 1));

        //rowtwo
        xStartTemp = xStart;
        yStartTemp = yStart - 16;
        wangTiles.Add(new WangTile(3, xStartTemp, yStartTemp, 1, 1, 1));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(0, 0, 592, 1, 0, 1));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(12, xStartTemp, yStartTemp, 1, 1, 1));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(11, xStartTemp, yStartTemp, 1, 1, 1));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(13, xStartTemp, yStartTemp, 1, 1, 1));

        //row three
        xStartTemp = xStart;
        yStartTemp = yStart - 32;
        wangTiles.Add(new WangTile(1, xStartTemp, yStartTemp, 1, 1, 1));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(9, xStartTemp, yStartTemp, 1, 1, 1));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(8, xStartTemp, yStartTemp, 1, 1, 1));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(10, xStartTemp, yStartTemp, 1, 1, 1));
        xStartTemp += 16;
        wangTiles.Add(new WangTile(5, xStartTemp, yStartTemp, 1, 1, 1));

        return Tuple.Create("Grass", wangTiles);
    }

}
