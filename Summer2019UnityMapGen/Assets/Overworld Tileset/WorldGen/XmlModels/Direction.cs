﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Assets.Overworld_Tileset.WorldGen.XmlModels
{
    public class Direction
    {
        [XmlAttribute("name")]
        public string Name;

        [XmlAttribute("path")]
        public string Path;
    }
}
