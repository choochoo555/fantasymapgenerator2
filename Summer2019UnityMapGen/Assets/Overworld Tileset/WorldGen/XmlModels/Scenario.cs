﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Assets.Overworld_Tileset.WorldGen.XmlModels
{
    [XmlRoot("Scenarios")]
    public class Scenario
    {
        [XmlAttribute("name")]
        public string Name;

        [XmlArray("Directions")]
        [XmlArrayItem("Direction")]
        public List<Direction> Directions = new List<Direction>();
    }
}