﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Assets.Overworld_Tileset.WorldGen.XmlModels
{
    [XmlRoot("MapTileData")]
    public class MapTileData
    {
        [XmlArray("Tiles")]
        [XmlArrayItem("Scenarios")]
        public List<Scenario> Scenarios = new List<Scenario>();
    }
}