﻿
using System.Linq;
using TileShared;
using UnityEngine;
using WorldTerrainShared;

namespace Assets.Overworld_Tileset.WorldGen
{
    public class WorldTile
    {
        public byte TerrainType;
        public WorldTile[] Neighbors = new WorldTile[8];
        public bool[] IsGreaterOrEqualTerrain = new bool[8];

        public int TopLeft { get; set; }
        public int TopRight { get; set; }
        public int BottomLeft { get; set; }
        public int BottomRight { get; set; }

        //public int X { get; set; }
        //public int Y { get; set; }

        public WorldTile(byte terrainType, int? testSum = null)
        {
            TerrainType = terrainType;
            if (testSum.HasValue)
                SetTiles(testSum.Value);
        }

        private void CalculateBlobSet()
        {
            var sum = 0;

            if (TerrainType == WorldTerrain.OceanDeepByte)
            {
                SetTiles(0);
                return;
            }

            if (IsGreaterOrEqualTerrain[(byte)Sides.West] && IsGreaterOrEqualTerrain[(byte)Sides.North] && IsGreaterOrEqualTerrain[(byte)Sides.NorthWest])
                sum += 128;
            if (IsGreaterOrEqualTerrain[(byte)Sides.North])
                sum += 1;
            if (IsGreaterOrEqualTerrain[(byte)Sides.East] && IsGreaterOrEqualTerrain[(byte)Sides.North] && IsGreaterOrEqualTerrain[(byte)Sides.NorthEast])
                sum += 2;

            if (IsGreaterOrEqualTerrain[(byte)Sides.West])
                sum += 64;
            if (IsGreaterOrEqualTerrain[(byte)Sides.East])
                sum += 4;

            if (IsGreaterOrEqualTerrain[(byte)Sides.West] && IsGreaterOrEqualTerrain[(byte)Sides.South] && IsGreaterOrEqualTerrain[(byte)Sides.SouthWest])
                sum += 32;
            if (IsGreaterOrEqualTerrain[(byte)Sides.South])
                sum += 16;
            if (IsGreaterOrEqualTerrain[(byte)Sides.East] && IsGreaterOrEqualTerrain[(byte)Sides.South] && IsGreaterOrEqualTerrain[(byte)Sides.SouthEast])
                sum += 8;

            var modSum = sum > 255 ? sum - 256 : sum;
            SetTiles(modSum);
            //Debug.Log("Terrain: " + TerrainType + " -Calc:" + modSum);
        }

        private void SetTiles(int sum)
        {
            switch (sum)
            {
                //Start One
                case 0:
                    TopLeft = TopRight = BottomLeft = BottomRight = 0;
                    break;
                //End One
                //Start Two
                case 1:
                    TopLeft = 3;
                    TopRight = 12;
                    BottomLeft = 1;
                    BottomRight = 8;
                    break;
                case 4:
                    TopLeft = 2;
                    TopRight = 6;
                    BottomLeft = 1;
                    BottomRight = 9;
                    break;
                case 16:
                    TopLeft = 2;
                    TopRight = 4;
                    BottomLeft = 3;
                    BottomRight = 12;
                    break;
                case 64:
                    TopLeft = 6;
                    TopRight = 4;
                    BottomLeft = 9;
                    BottomRight = 8;
                    break;
                //End Two
                //Start Three
                case 5:
                    TopLeft = 3;
                    TopRight = 14;
                    BottomLeft = 1;
                    BottomRight = 9;
                    break;
                case 20:
                    TopLeft = 2;
                    TopRight = 6;
                    BottomLeft = 3;
                    BottomRight = 13;
                    break;
                case 80:
                    TopLeft = 6;
                    TopRight = 4;
                    BottomLeft = 11;
                    BottomRight = 12;
                    break;
                case 65:
                    TopLeft = 7;
                    TopRight = 12;
                    BottomLeft = 9;
                    BottomRight = 8;
                    break;
                //End Three
                //Start Four
                case 7:
                    TopLeft = 3;
                    TopRight = 15;
                    BottomLeft = 1;
                    BottomRight = 9;
                    break;
                case 28:
                    TopLeft = 2;
                    TopRight = 6;
                    BottomLeft = 3;
                    BottomRight = 15;
                    break;
                case 112:
                    TopLeft = 6;
                    TopRight = 4;
                    BottomLeft = 15;
                    BottomRight = 12;
                    break;
                case 193:
                    TopLeft = 15;
                    TopRight = 12;
                    BottomLeft = 9;
                    BottomRight = 8;
                    break;
                //End Four
                //Start Five
                case 17:
                    TopLeft = 3;
                    TopRight = 12;
                    BottomLeft = 3;
                    BottomRight = 12;
                    break;
                case 68:
                    TopLeft = 6;
                    TopRight = 6;
                    BottomLeft = 9;
                    BottomRight = 9;
                    break;
                //End Five
                //Start Six
                case 21:
                    TopLeft = 3;
                    TopRight = 14;
                    BottomLeft = 3;
                    BottomRight = 13;
                    break;
                case 84:
                    TopLeft = 6;
                    TopRight = 6;
                    BottomLeft = 11;
                    BottomRight = 13;
                    break;
                case 81:
                    TopLeft = 7;
                    TopRight = 12;
                    BottomLeft = 11;
                    BottomRight = 12;
                    break;
                case 69:
                    TopLeft = 7;
                    TopRight = 14;
                    BottomLeft = 9;
                    BottomRight = 9;
                    break;
                //End Six
                //Start Seven
                case 23:
                    TopLeft = 3;
                    TopRight = 15;
                    BottomLeft = 3;
                    BottomRight = 13;
                    break;
                case 92:
                    TopLeft = 6;
                    TopRight = 6;
                    BottomLeft = 11;
                    BottomRight = 15;
                    break;
                case 113:
                    TopLeft = 7;
                    TopRight = 12;
                    BottomLeft = 15;
                    BottomRight = 12;
                    break;
                case 197:
                    TopLeft = 15;
                    TopRight = 14;
                    BottomLeft = 9;
                    BottomRight = 9;
                    break;
                //End Seven
                //Start Eight
                case 29:
                    TopLeft = 3;
                    TopRight = 14;
                    BottomLeft = 3;
                    BottomRight = 15;
                    break;
                case 116:
                    TopLeft = 6;
                    TopRight = 6;
                    BottomLeft = 15;
                    BottomRight = 13;
                    break;
                case 209:
                    TopLeft = 15;
                    TopRight = 12;
                    BottomLeft = 11;
                    BottomRight = 12;
                    break;
                case 71:
                    TopLeft = 7;
                    TopRight = 15;
                    BottomLeft = 9;
                    BottomRight = 9;
                    break;
                //End Eight
                //Start Nine
                case 31:
                    TopLeft = 3;
                    TopRight = 15;
                    BottomLeft = 3;
                    BottomRight = 15;
                    break;
                case 124:
                    TopLeft = 6;
                    TopRight = 6;
                    BottomLeft = 15;
                    BottomRight = 15;
                    break;
                case 241:
                    TopLeft = 15;
                    TopRight = 12;
                    BottomLeft = 15;
                    BottomRight = 12;
                    break;
                case 199:
                    TopLeft = 15;
                    TopRight = 15;
                    BottomLeft = 9;
                    BottomRight = 9;
                    break;
                //End Nine
                //Start Ten
                case 85:
                    TopLeft = 7;
                    TopRight = 14;
                    BottomLeft = 11;
                    BottomRight = 13;
                    break;
                //End Ten
                //Start Eleven
                case 87:
                    TopLeft = 7;
                    TopRight = 15;
                    BottomLeft = 11;
                    BottomRight = 13;
                    break;
                case 93:
                    TopLeft = 7;
                    TopRight = 14;
                    BottomLeft = 11;
                    BottomRight = 15;
                    break;
                case 117:
                    TopLeft = 7;
                    TopRight = 14;
                    BottomLeft = 15;
                    BottomRight = 13;
                    break;
                case 213:
                    TopLeft = 15;
                    TopRight = 14;
                    BottomLeft = 11;
                    BottomRight = 13;
                    break;
                //End Eleven
                //Start Twelve
                case 95:
                    TopLeft = 7;
                    TopRight = 15;
                    BottomLeft = 11;
                    BottomRight = 15;
                    break;
                case 125:
                    TopLeft = 7;
                    TopRight = 14;
                    BottomLeft = 15;
                    BottomRight = 15;
                    break;
                case 245:
                    TopLeft = 15;
                    TopRight = 14;
                    BottomLeft = 15;
                    BottomRight = 13;
                    break;
                case 215:
                    TopLeft = 15;
                    TopRight = 15;
                    BottomLeft = 11;
                    BottomRight = 13;
                    break;
                //End Twelve
                //Start Thirteen
                case 119:
                    TopLeft = 7;
                    TopRight = 15;
                    BottomLeft = 15;
                    BottomRight = 13;
                    break;
                case 221:
                    TopLeft = 15;
                    TopRight = 14;
                    BottomLeft = 11;
                    BottomRight = 15;
                    break;
                //End Thirteen
                //Start Fourteen
                case 127:
                    TopLeft = 7;
                    TopRight = 15;
                    BottomLeft = 15;
                    BottomRight = 15;
                    break;
                case 253:
                    TopLeft = 15;
                    TopRight = 14;
                    BottomLeft = 15;
                    BottomRight = 15;
                    break;
                case 247:
                    TopLeft = 15;
                    TopRight = 15;
                    BottomLeft = 15;
                    BottomRight = 13;
                    break;
                case 223:
                    TopLeft = 15;
                    TopRight = 15;
                    BottomLeft = 11;
                    BottomRight = 15;
                    break;
                //End Fourteen
                //End Fifteen
                case 255:
                    TopLeft = 15;
                    TopRight = 15;
                    BottomLeft = 15;
                    BottomRight = 15;
                    break;
                    //End Fifteen
            }
        }

        public void Reset()
        {
            for (var i = 0; i < Neighbors.Length; i++)
                Neighbors[i] = null;
        }

        
        public void SetNeighbors(WorldTile[][] tileData, int x, int y)
        {
            Neighbors = TileTools.GetNeighborsTwoD<WorldTile>(tileData, x, y);

            IsGreaterOrEqualTerrain = TileShared.Sides.SidesCollection.ToList()
                                        .Select(side => Neighbors[side])
                                        .Select(tile => tile == null || tile.TerrainType >= this.TerrainType)
                                        .ToArray();

            CalculateBlobSet();
        }
        

    }
}
