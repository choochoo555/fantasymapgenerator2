﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Overworld_Tileset.WorldGen.Enums
{
    public enum TileSquareEnum
    {
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight
    }
}
