﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;
using WorldGeneratorShared;
using WorldTerrainShared;
using Random = UnityEngine.Random;

namespace Assets.Overworld_Tileset.WorldGen
{
    public class WorldGenerator : MonoBehaviour
    {
        private Tilemap _tilemap;
        private int[,] MapData = null;
        private WorldTile[][] TileData;
        private WorldTerrainFetches _terrains;
        private WorldProperties _wp;

        // Start is called before the first frame update
        void Start()
        {
            _wp = new WorldProperties(800,600,300);
            MapData = new int[_wp.Width, _wp.Height];
            _tilemap = GameObject.Find("MapGridTilemap").GetComponent<Tilemap>();
            _terrains = new WorldTerrainFetches();

            var mapDataLoc = "Assets/MapData/Slot1";
            var zipLoc = @"Assets\Plugins\";
            var wg = new WorldGenInit(_wp, mapDataLoc, zipLoc);
            wg.GenerateAndSave();
            
            for (int x = 0; x < _wp.Width; x++)
                for (int y = 0; y < _wp.Height; y++)
                    MapData[x, y] = _wp.MapData[y * _wp.Width + x];

            /*
            MapData = new int[4, 4]
            {
                {0,0,1,0},
                {1,1,1,1},
                {0,0,1,0},
                {1,1,1,1}
            };
            */
            TileData = new WorldTile[MapData.GetLength(0)][];
            //TileData = TestCombinations();

            //create neighbors.
            for (int x = 0; x < TileData.Length; x++)
            {
                TileData[x] = new WorldTile[MapData.GetLength(1)];
                for (int y = 0; y < TileData[x].Length; y++)
                {
                    TileData[x][y] = new WorldTile((byte)MapData[x, y]);
                }
            }

            //assign neighbors.
            for (int x = 0; x < TileData.Length; x++)
            {
                for (int y = 0; y < TileData[x].Length; y++)
                {
                    TileData[x][y].SetNeighbors(TileData, x, y);
                }
            }

            for (int x = 0; x < TileData.Length; x++)
            {
                for (int y = 0; y < TileData[x].Length; y++)
                {

                    var currentTd = TileData[x][y];
                    var foundCollection = _terrains.Types.FirstOrDefault(t => t.Item2 == currentTd.TerrainType);
                    if (foundCollection == null)
                        continue;

                    var tileSet = foundCollection.Item3;
                    var worldX = x * 2;
                    var worldY = y * 2;

                    var topLeft = tileSet[currentTd.TopLeft];
                    if (topLeft != null)
                        _tilemap.SetTile(new Vector3Int(worldX, -worldY, 0), topLeft);

                    var topRight = tileSet[currentTd.TopRight];
                    if (topRight != null)
                        _tilemap.SetTile(new Vector3Int(worldX + 1, -worldY, 0), topRight);

                    var bottomRight = tileSet[currentTd.BottomRight];
                    if (bottomRight != null)
                        _tilemap.SetTile(new Vector3Int(worldX + 1, -worldY - 1, 0), bottomRight);

                    var bottomLeft = tileSet[currentTd.BottomLeft];
                    if (bottomLeft != null)
                        _tilemap.SetTile(new Vector3Int(worldX, -worldY - 1, 0), bottomLeft);

                }
            }

        }

        private WorldTile[][] TestCombinations()
        {
            //testing all scenarios for wang blob tileset
            WorldTile[][] wts = new WorldTile[15][];

            var oneDIndex = -1;
            //one
            wts[++oneDIndex] = new WorldTile[1];
            wts[oneDIndex][0] = new WorldTile(0, 0);

            //two
            wts[++oneDIndex] = new WorldTile[4];
            wts[oneDIndex][0] = new WorldTile(0, 1);
            wts[oneDIndex][1] = new WorldTile(0, 4);
            wts[oneDIndex][2] = new WorldTile(0, 16);
            wts[oneDIndex][3] = new WorldTile(0, 64);

            //three
            wts[++oneDIndex] = new WorldTile[4];
            wts[oneDIndex][0] = new WorldTile(0, 5);
            wts[oneDIndex][1] = new WorldTile(0, 20);
            wts[oneDIndex][2] = new WorldTile(0, 80);
            wts[oneDIndex][3] = new WorldTile(0, 65);

            //four
            wts[++oneDIndex] = new WorldTile[4];
            wts[oneDIndex][0] = new WorldTile(0, 7);
            wts[oneDIndex][1] = new WorldTile(0, 28);
            wts[oneDIndex][2] = new WorldTile(0, 112);
            wts[oneDIndex][3] = new WorldTile(0, 193);

            //five
            wts[++oneDIndex] = new WorldTile[2];
            wts[oneDIndex][0] = new WorldTile(0, 17);
            wts[oneDIndex][1] = new WorldTile(0, 68);

            //six
            wts[++oneDIndex] = new WorldTile[4];
            wts[oneDIndex][0] = new WorldTile(0, 21);
            wts[oneDIndex][1] = new WorldTile(0, 84);
            wts[oneDIndex][2] = new WorldTile(0, 81);
            wts[oneDIndex][3] = new WorldTile(0, 69);

            //seven
            wts[++oneDIndex] = new WorldTile[4];
            wts[oneDIndex][0] = new WorldTile(0, 23);
            wts[oneDIndex][1] = new WorldTile(0, 92);
            wts[oneDIndex][2] = new WorldTile(0, 113);
            wts[oneDIndex][3] = new WorldTile(0, 197);

            //eight
            wts[++oneDIndex] = new WorldTile[4];
            wts[oneDIndex][0] = new WorldTile(0, 29);
            wts[oneDIndex][1] = new WorldTile(0, 116);
            wts[oneDIndex][2] = new WorldTile(0, 209);
            wts[oneDIndex][3] = new WorldTile(0, 71);

            //nine
            wts[++oneDIndex] = new WorldTile[4];
            wts[oneDIndex][0] = new WorldTile(0, 31);
            wts[oneDIndex][1] = new WorldTile(0, 124);
            wts[oneDIndex][2] = new WorldTile(0, 241);
            wts[oneDIndex][3] = new WorldTile(0, 199);

            //ten
            wts[++oneDIndex] = new WorldTile[1];
            wts[oneDIndex][0] = new WorldTile(0, 85);

            //eleven
            wts[++oneDIndex] = new WorldTile[4];
            wts[oneDIndex][0] = new WorldTile(0, 87);
            wts[oneDIndex][1] = new WorldTile(0, 93);
            wts[oneDIndex][2] = new WorldTile(0, 117);
            wts[oneDIndex][3] = new WorldTile(0, 213);

            //twelve
            wts[++oneDIndex] = new WorldTile[4];
            wts[oneDIndex][0] = new WorldTile(0, 95);
            wts[oneDIndex][1] = new WorldTile(0, 125);
            wts[oneDIndex][2] = new WorldTile(0, 245);
            wts[oneDIndex][3] = new WorldTile(0, 215);

            //twelve
            wts[++oneDIndex] = new WorldTile[2];
            wts[oneDIndex][0] = new WorldTile(0, 119);
            wts[oneDIndex][1] = new WorldTile(0, 221);

            //thirteen
            wts[++oneDIndex] = new WorldTile[4];
            wts[oneDIndex][0] = new WorldTile(0, 127);
            wts[oneDIndex][1] = new WorldTile(0, 253);
            wts[oneDIndex][2] = new WorldTile(0, 247);
            wts[oneDIndex][3] = new WorldTile(0, 223);

            //fuorteen
            wts[++oneDIndex] = new WorldTile[1];
            wts[oneDIndex][0] = new WorldTile(0, 255);

            return wts;
        }

        private void PrintArray(int[,] mapdata)
        {
            for (int x = 0; x < mapdata.GetLength(0); x++)
            {
                var line = string.Empty;
                for (int y = 0; y < mapdata.GetLength(1); y++)
                {
                    line += mapdata[x, y] + ",";
                }
                Debug.Log(line);
            }
        }

    }
}
