﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Assets.Overworld_Tileset.WorldGen.XmlModels;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;
using WorldTerrainShared;

namespace Assets.Overworld_Tileset.WorldGen
{
    public class WorldTerrainFetches
    {
        public readonly List<Tuple<string, int, AnimatedTile[]>> Types = new List<Tuple<string, int, AnimatedTile[]>>();


        public WorldTerrainFetches()
        {
            Types.Add(Tuple.Create(WorldTerrain.OceanDeep, WorldTerrain.OceanDeepByte, ImportTiles(WorldTerrain.OceanDeep)));
            Types.Add(Tuple.Create(WorldTerrain.Ocean, WorldTerrain.OceanByte, ImportTiles(WorldTerrain.Ocean)));
            Types.Add(Tuple.Create(WorldTerrain.OceanShallow, WorldTerrain.OceanShallowByte, ImportTiles(WorldTerrain.OceanShallow)));
            Types.Add(Tuple.Create(WorldTerrain.Grass, WorldTerrain.GrassByte, ImportTiles(WorldTerrain.Grass)));
        }

        private AnimatedTile[] ImportTiles(string name)
        {
            var path = "Animated Tiles/" + name;
            var tiles = Resources.LoadAll<AnimatedTile>(path);
            var tilesList = new List<AnimatedTile>(tiles);
            return tilesList.OrderBy(x => Int16.Parse(x.name)).ToArray();
        }
    }
}
